

#include <R.h>


#include "../../../include/Resampler/Hilbert_Resampler/HilbertResamplerQMC.h"
#include "../../../include/Resampler/Resampler.h"
#include "../../../include/functionsCC.hpp"

#include "../../../include/Models/Model_LG.h"
#include "../../../include/Models/Model_SV.h"

#include "../../../include/SQMC.h"
#include "../../../include/SQMC_Back.h"
#include "../../../include/SMC.h"
#include "../../../include/SMC_Back.h"

/*******************************************************************************************************************************************/

void SQMC_SV(double*, int*, int*, int*, double*, int*, int*, int*, int*,  int*, int*,double*, double*, double*, double*);

void SQMCBack_SV(double*, int*, int*, int*, double*, int*, int*, int*, int*, int*, int*, int*, int*, double*, double*, double*, double*);

void SQMC2F_SV(double*, int*, int*, int*, int*, double*, double*, int*, int*, int*, int*, int*, int*, double*,  double*, double*, double*);
/************************* PARTICLE FILTER ***************************************************************************************************/


void SQMC_SV(double *y, int *dy, int *dx, int *T, double *theta, int *seed, int *ns, int *N, int *qmc, int *src, int *ExtraParam,
double *parPsi, double *lik, double *expx, double *expx2)
{	
	int i,j,k;
	int par[3]={ExtraParam[0], *src, ExtraParam[1]};
	
	if(par[0]==1)
	{
		for(j=0;j<*T;j++)
		{
			expx[j]=0;
			expx2[j]=0;
		}
	}
	
	gsl_rng *random=gsl_rng_alloc (gsl_rng_mt19937);

	if(*seed>0)
	{
		gsl_rng_set(random, *seed);
	}
	else
	{
		gsl_rng_set(random, getSeed());
	}

	double *storeExp=(double*)malloc(sizeof(double)*(*dx*(*T)));
	double *v1=(double*)malloc(sizeof(double)*(*T));

	for(i=0;i<*ns;i++)
	{
		if(*qmc!=0)
		{
			SQMC(y, *dy, *dx, *T, theta, *N, *qmc, paramTransQMC_SV, HilbertResampler, 
			simPriorQMC_SV, simTransitionQMC_LG,potential_SV, par, parPsi, v1,storeExp);
		}
		else	
		{
			SMC(y, *dy, *dx, *T, theta, *N, random, paramTrans_SV, ResamplePF, simPrior_SV,  
			simTransition_LG, potential_SV, par,v1,storeExp);
		}

		for(j=0;j<*T;j++)
		{
			lik[*T*i+j]=v1[j];
		}

		if(par[0]==1)
		{
			for(j=0;j<*T;j++)
			{
				for(k=0;k<*dx;k++)
				{
					expx[*dx*j+k]+=storeExp[*dx*j+k]/(*ns);
					expx2[*dx*j+k]+=pow(storeExp[*dx*j+k],2)/(*ns);
				}
			}
		}	
	}
	
	free(storeExp);
	storeExp=NULL;
	free(v1);
	v1=NULL;
	gsl_rng_free(random);
	random=NULL;
}


	

void SQMCBack_SV(double *y, int *dy, int *dx, int *T, double *theta, int *seed, int *ns, int *N, int *Nb, int *qmc, int *qmcB,  int *Marg, int *CompPsi, double *parPsi, 
double *lik, double *expx, double *expx2)
{	
	int i, j, k,  n=(*N)*(*dx);
	double cc;
	
	double *W=(double*)malloc(sizeof(double)*(*N));
	double *storeW=(double*)malloc(sizeof(double)*(*N*(*T)));

	double *x=(double*)malloc(sizeof(double)*(n));
	double *storeX=(double*)malloc(sizeof(double)*((*T)*n));

	double *xT=(double*)malloc(sizeof(double)*(*Nb*(*dx)));
	
	int par[3]={1, 1,*CompPsi};
	
	for(j=0;j<*T;j++)
	{
		for(k=0;k<*dx;k++)
		{
			expx[*dx*j+k]=0;
			expx2[*dx*j+k]=0;
		}
	}
	
	
	gsl_rng *random=gsl_rng_alloc (gsl_rng_mt19937);

	if(*seed>0)
	{
		gsl_rng_set(random, *seed);
	}
	else
	{
		gsl_rng_set(random, getSeed());
	}

	double *storeExp=(double*)malloc(sizeof(double)*(*dx*(*T)));
	double *v1=(double*)malloc(sizeof(double)*(*T));

	for(i=0;i<*ns;i++)
	{
		if(*qmc!=0)
		{
		
			SQMC_Forward(y, *dy, *dx, *T, theta, *N,  *qmc, paramTransQMC_SV, ForHilbertResampler,simPriorQMC_SV,
			simTransitionQMC_LG, potential_SV, par, parPsi, v1, storeX, storeW, x, W);

			if(*Marg==0)
			{
				if(*qmcB==0)
				{
					SMC_Back(*dx, *T, 0, theta, *N, *Nb, random, ResamplePF, transition_SV, x, storeX, W, storeW, storeExp, xT);
				}
				else
				{
					SQMC_Back(*dx, *T, 0, theta, *N, *Nb, *qmcB, BackHilbertResampler, transition_SV, parPsi, x,
					storeX, W, storeW,  storeExp , xT);
				}
			}else
			{
				SMC_BackMarg(*dx, *T,  theta, *N,  transition_SV, x, storeX, W, storeW, storeExp);
				
			}
		}
		else
		{
			if(*Marg==0)
			{
				SMC_ForBack(y, *dy, *dx, *T, theta, *N, *Nb, random, paramTrans_SV, ResamplePF, simPrior_SV,  
				transition_SV,  simTransition_LG, potential_SV, par, v1,storeExp);
			}
			else
			{
				SMC_ForBackMarg(y, *dy, *dx, *T, theta, *N,  random, paramTrans_SV, ResamplePF, simPrior_SV,  
				transition_SV,  simTransition_LG, potential_SV, par, v1,storeExp);
			}	
		}
		

		for(j=0;j<*T;j++)
		{
			lik[*T*i+j]=v1[j];
		}

		for(j=0;j<*T;j++)
		{
			for(k=0;k<*dx;k++)
			{
				expx[*dx*j+k]+=storeExp[*dx*j+k]/(*ns);
				expx2[*dx*j+k]+=pow(storeExp[*dx*j+k],2)/(*ns);
			}
		}
			
	}

	free(W);
	W=NULL;
	free(storeW);
	storeW=NULL;
	free(x);
	x=NULL;
	free(storeX);
	storeX=NULL;
	free(xT);
	xT=NULL;

	free(storeExp);
	storeExp=NULL;
	free(v1);
	v1=NULL;
	gsl_rng_free(random);
	random=NULL;
}






void SQMC2F_SV(double *y, int *dy, int *dx, int *tstar, int *T, double *theta, double *theta2F, int *seed, int *ns, int *N, int *Nb, int *qmc, int *CompPsi,
double *parPsi, double *lik, double *expx, double *expx2)
{	
	int i,j,k;
	int par[3]={1, 1, *CompPsi};
	
	for(j=0;j<*T;j++)
	{
		for(k=0;k<*dx;k++)
		{
			expx[*dx*j+k]=0;
			expx2[*dx*j+k]=0;
		}
	}
	
	gsl_rng *random=gsl_rng_alloc (gsl_rng_mt19937);

	if(*seed>0)
	{
		gsl_rng_set(random, *seed);
	}
	else
	{
		gsl_rng_set(random, getSeed());
	}

	double *storeExp=(double*)malloc(sizeof(double)*(*dx*(*T)));
	double *v1=(double*)malloc(sizeof(double)*(*T));

	for(i=0;i<*ns;i++)
	{
		if(*qmc!=0)
		{	
			SQMC_2F(y, *tstar, *dy, *dx, *T, theta, theta2F, *N, *Nb, *qmc, paramTransQMC_SV, ForHilbertResampler,
			BackHilbertResampler, simPriorQMC_SV, transition_SV, simTransitionQMC_LG, 
			potential_SV, paramTransQMC_SV,  simPriorQMC_SV, transition_SV, simTransitionQMC_LG, 
			potential_SV,  par, parPsi, v1, storeExp);
		}
		else
		{
			SMC_2F(y, *tstar, *dy, *dx, *T, theta, theta2F, *N, *Nb,  random, paramTrans_SV, ResamplePF, simPrior_SV, 
			transition_SV, simTransition_LG, potential_SV, paramTrans_SV, simPrior_SV, transition_SV,
			simTransition_LG, potential_SV,  par, v1, storeExp);
		}
	
		for(j=0;j<*T;j++)
		{
			lik[*T*i+j]=v1[j];
		}

		for(j=0;j<*T;j++)
		{
			for(k=0;k<*dx;k++)
			{
				expx[*dx*j+k]+=storeExp[*dx*j+k]/(*ns);
				expx2[*dx*j+k]+=pow(storeExp[*dx*j+k],2)/(*ns);
			}
		}
	}
			

	free(storeExp);
	storeExp=NULL;
	free(v1);
	v1=NULL;
	gsl_rng_free(random);
	random=NULL;
		
	
}












