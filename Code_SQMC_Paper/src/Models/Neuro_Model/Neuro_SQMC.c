


#include <R.h>


#include "../../../include/Resampler/Hilbert_Resampler/HilbertResamplerQMC.h"
#include "../../../include/Resampler/Resampler.h"
#include "../../../include/functionsCC.hpp"

#include "../../../include/Models/Model_LG.h"
#include "../../../include/Models/Model_Neuro.h"

#include "../../../include/SQMC.h"
#include "../../../include/SMC.h"




/*******************************************************************************************************************************************/

void SQMC_Neuro(double*, int*, int*, int*, double*, int*, int*, int*, int*,  int*, int*,double*, double*, double*, double*);

/************************* PARTICLE FILTER ***************************************************************************************************/


void SQMC_Neuro(double *y, int *dy, int *dx, int *T, double *theta, int *seed, int *ns, int *N, int *qmc, int *src, int *ExtraParam,
double * parPsi, double *lik, double *expx, double *expx2)
{
	int i,j,k;
	int par[3]={ExtraParam[0], *src,ExtraParam[1]};
	
	if(par[0]==1 && *ns!=1)
	{
		for(j=0;j<*T;j++)
		{
			expx[j]=0;
			expx2[j]=0;
		}
	}

	gsl_rng *random=gsl_rng_alloc (gsl_rng_taus);

	if(*seed>0)
	{
		gsl_rng_set(random, *seed);
	}
	else
	{
		gsl_rng_set(random, getSeed());
	}

	double *storeExp=(double*)malloc(sizeof(double)*(*dx*(*T)));
	double *v1=(double*)malloc(sizeof(double)*(*T));

	
	for(i=0;i<*ns;i++)
	{
		if(*ns==1)
		{
			if(*qmc!=0)
			{
				SQMC(y, *dy, *dx, *T, theta, *N, *qmc, paramTransQMC_Neuro, HilbertResampler, 
				simPriorQMC_LG, simTransitionQMC_Neuro,potential_Neuro, par, parPsi, lik,expx);
			}
			else
			{
				SMC(y, *dy, *dx, *T, theta, *N, random, paramTrans_Neuro, ResamplePF, simPrior_LG,  
				simTransition_Neuro, potential_Neuro, par,lik,expx);
			}
		}
		else
		{
			if(*qmc!=0)
			{
				SQMC(y, *dy, *dx, *T, theta, *N, *qmc, paramTransQMC_Neuro, HilbertResampler, 
				simPriorQMC_LG, simTransitionQMC_Neuro,potential_Neuro, par, parPsi, v1,storeExp);
			}
			else
			{
				SMC(y, *dy, *dx, *T, theta, *N, random, paramTrans_Neuro, ResamplePF, simPrior_LG,  
				simTransition_Neuro, potential_Neuro, par,v1,storeExp);
			}

			for(j=0;j<*T;j++)
			{
				lik[*T*i+j]=v1[j];
			}

			if(par[0]==1)
			{
				for(j=0;j<*T;j++)
				{
					for(k=0;k<*dx;k++)
					{
						expx[*dx*j+k]+=storeExp[*dx*j+k]/(*ns);
						expx2[*dx*j+k]+=pow(storeExp[*dx*j+k],2)/(*ns);
					}
				}
			}
		}	
	}

	free(storeExp);
	storeExp=NULL;
	free(v1);
	v1=NULL;
	gsl_rng_free(random);
	random=NULL;
}













