
#include <R.h>


#include "../../../include/Resampler/Hilbert_Resampler/HilbertResamplerQMC.h"
#include "../../../include/Resampler/Resampler.h"
#include "../../../include/functionsCC.hpp"

#include "../../../include/Models/Model_Univ.h"

#include "../../../include/SQMC.h"
#include "../../../include/SQMC_Back.h"
#include "../../../include/SMC.h"
#include "../../../include/SMC_Back.h"

/*******************************************************************************************************************************************/

void SQMC_Univ(double*, int*, int*, int*, double*, int*, int*, int*, int*, int*, int*, double*, double*, double*);

void SQMCBack_Univ(double*, int*, int*, int*, double*, int*, int*, int*, int*, int*, int*, int*,  double*, double*, double*);

/***********************************************************************************************/

	
void SQMC_Univ(double *y, int *dy, int *dx, int *T, double *theta, int *seed, int *ns, int *N, int *qmc, int* src, int *computeExp,
double *lik, double *expx, double *expx2)
{
	int i,j;
	int par[3]={*computeExp, *src,0};
	double *parPsi=NULL;
	
	if(par[0]==1)
	{
		for(j=0;j<*T;j++)
		{
			expx[j]=0;
			expx2[j]=0;
		}
	}

	gsl_rng *random=gsl_rng_alloc (gsl_rng_mt19937);

	if(*seed>0)
	{
		gsl_rng_set(random, *seed);
	}
	else
	{
		gsl_rng_set(random, getSeed());
	}

	double *storeExp=(double*)malloc(sizeof(double)*(*T));
	double *v1=(double*)malloc(sizeof(double)*(*T));

	for(i=0;i<*ns;i++)
	{
		if(*qmc!=0)
		{
			SQMC(y, *dy, *dx, *T, theta, *N, *qmc, paramTrans_Univ, HilbertResampler, 
			simPriorQMC_Univ, simTransitionQMC_Univ, potential_Univ, par,parPsi, v1,storeExp);
		}
		else
		{
			SMC(y, *dy, *dx, *T, theta, *N, random, paramTrans_Univ, ResamplePF, simPrior_Univ, simTransition_Univ, 
			potential_Univ, par,v1,storeExp);
		}
		
		for(j=0;j<*T;j++)
		{
			lik[*T*i+j]=v1[j];
		}

		if(par[0]==1)
		{
			for(j=0;j<*T;j++)
			{
				expx[j]+=storeExp[j]/(*ns);
				expx2[j]+=pow(storeExp[j],2)/(*ns);
			}
		}
	}

	free(storeExp);
	storeExp=NULL;
	free(v1);
	v1=NULL;
	gsl_rng_free(random);
	random=NULL;
}




void SQMCBack_Univ(double *y, int *dy, int *dx, int *T, double *theta, int *seed, int *ns, int *N, int *Nb, int *qmc, int *qmcb,  int *M, double *lik, 
double *expx, double *expx2)
{	
	int i, j, k;
	double cc, *parPsi=NULL;
	
	double *W=(double*)malloc(sizeof(double)*(*N));
	double *storeW=(double*)malloc(sizeof(double)*(*N*(*T)));

	double *x=(double*)malloc(sizeof(double)*(*N));
	double *storeX=(double*)malloc(sizeof(double)*((*T)*(*N)));

	double *xT=(double*)malloc(sizeof(double)*(*Nb));
	
	int par[3]={1, 1,0};
	
	if(*ns!=1)
	{
		for(j=0;j<*T;j++)
		{
			for(k=0;k<*dx;k++)
			{
				expx[*dx*j+k]=0;
				expx2[*dx*j+k]=0;
			}
		}
	}

	gsl_rng *random=gsl_rng_alloc (gsl_rng_mt19937);

	if(*seed>0)
	{
		gsl_rng_set(random, *seed);
	}
	else
	{
		gsl_rng_set(random, getSeed());
	}

	double *storeExp=(double*)malloc(sizeof(double)*(*dx*(*T)));
	double *v1=(double*)malloc(sizeof(double)*(*T));

	for(i=0;i<*ns;i++)
	{
		if(*qmc!=0)
		{
			SQMC_Forward(y, *dy, *dx, *T, theta, *N,  *qmc, paramTrans_Univ, ForHilbertResampler,simPriorQMC_Univ,
			simTransitionQMC_Univ, potential_Univ, par, parPsi, v1, storeX, storeW, x, W);		

			if(*M==0)
			{
				if(*qmcb==0)
				{
					SMC_Back(*dx, *T, 0, theta, *N, *Nb, random, ResamplePF, transition_Univ, x, storeX, W, storeW, storeExp, xT);
				}
				else
				{
					SQMC_Back(*dx, *T, 0, theta, *N, *Nb, *qmcb, BackHilbertResampler, transition_Univ, parPsi, x,
					storeX, W, storeW,  storeExp , xT);
				}
			}
			else
			{
				SMC_BackMarg(*dx, *T,  theta, *N,  transition_Univ, x, storeX, W, storeW, storeExp);
				
			}
		}
		else
		{
			if(*M==0)
			{
				SMC_ForBack(y, *dy, *dx, *T, theta, *N, *Nb, random, paramTrans_Univ, ResamplePF, simPrior_Univ,  
				transition_Univ,  simTransition_Univ, potential_Univ, par, v1,storeExp);
			}
			else
			{
				SMC_ForBackMarg(y, *dy, *dx, *T, theta, *N,  random, paramTrans_Univ, ResamplePF, simPrior_Univ,  
				transition_Univ,  simTransition_Univ, potential_Univ, par, v1,storeExp);
			}	
		}

		for(j=0;j<*T;j++)
		{
			lik[*T*i+j]=v1[j];
		}

		for(j=0;j<*T;j++)
		{
			expx[j]+=storeExp[j]/(*ns);
			expx2[j]+=pow(storeExp[j],2)/(*ns);
		}
	}

	free(W);
	W=NULL;
	free(storeW);
	storeW=NULL;
	free(x);
	x=NULL;
	free(storeX);
	storeX=NULL;
	free(xT);
	xT=NULL;

	free(storeExp);
	storeExp=NULL;
	free(v1);
	v1=NULL;
	gsl_rng_free(random);
	random=NULL;
		
	
}

























