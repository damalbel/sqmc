


#include<stdlib.h>  		
#include<stdio.h>	


#include "generate_RQMC.hpp"	

typedef void *DigitalNetGenerator;

typedef void *Scrambled;

void Scrambling(Scrambled*);

void getPoints(Scrambled*, int*, int*, double*);
 
Scrambled * Scrambled_Create(int*,int*, int*);

void Scrambled_Destroy(Scrambled*);

void Scrambled_Randomize(Scrambled*);

void Scrambled_GetPoints(Scrambled*, int*, int*, double*);



DigitalNetGenerator *DigitalNetGenerator_Create(int*, int*);

void DigitalNetGenerator_Destroy(DigitalNetGenerator*); 

void DigitalNetGenerator_GetPoints(DigitalNetGenerator*, int*, int*, double*); 

void Digital_getPoints(DigitalNetGenerator*, int*, int*, double*); 


void RQMC(int *qmc, int *N, int *dx, int *ns, double* x);

void QMC(int *qmc, int *N, int *dx, double* x);
























