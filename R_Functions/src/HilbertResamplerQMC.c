

#include "../include/HilbertResamplerQMC.h"
#include "../include/qsort_index.h"

	

/****************************************************/
 
void HilbertResamplerQMC(double *quasi, double *parPhi, double *x, int *dx, int *N, double *W, double* J)
{
	int i,k;
	uint64_t *h=NULL;
	int *J1=NULL;

	if(*dx==1)
	{
		J1=qsort_index( x, *N, sizeof(double), compare_double);
	}
	else
	{
		h=(uint64_t*)malloc(sizeof(uint64_t)*(*N));

		HilbertIndex(x, parPhi, dx, N , h);		
	
		J1=qsort_index(h, *N, sizeof(uint64_t), compare_uint64);

		free(h);
		h=NULL;
	}

	quasi_Resample(quasi, N, W, J1, x, J);

	free(J1);
	J1=NULL;
}


/****************************************************/

void quasi_Resample(double *quasi,  int *N, double *W, int *J1, double *x, double *J)
{
	int i,j,k;
	double s;
	
    	s=0;
	j=0;

	for(i=0; i<*N; i++)
	{
		s+=W[J1[i]];

		while(quasi[j]<=s && j<*N)
		{
			J[j]=J1[i]+1;
			
			j++;

		}
		if(j==*N)
		{
			break;
		}
	     
	}
}



int compare_double (const void *pa, const void *pb)
{
	const double *a= pa;
	const double *b= pb;
        if (a[0] > b[0])
                return 1;
        else if (a[0] < b[0])
                 return -1;
       	else
                 return 0;
}


int compare_uint64(const void *pa, const void *pb)
{
	uint64_t a = *(const uint64_t*)pa;
   	uint64_t b = *(const uint64_t*)pb;

        if (a > b)
	{
               	return 1;
	}
       	else if (a < b)
        {
		return -1;
	}
        else
	{
        	return 0;
	}
}




		
	

