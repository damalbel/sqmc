
#include "../include/Scrambling.h"	


void Scrambling(Scrambled* os ) 
{
    Scrambled_Randomize(os);
}


void getPoints(Scrambled* os, int *d, int *n, double *seq) 
{
    Scrambled_GetPoints(os, d, n, seq);
}



void Digital_getPoints(DigitalNetGenerator* os, int *d, int *n, double *seq) 
{
    DigitalNetGenerator_GetPoints(os, d, n, seq);
}

/****************************************************************************************************/


void RQMC(int *qmc, int *N, int *dx, int *ns, double* x)
{
	int i,k;
	
	Scrambled *os1=Scrambled_Create(qmc, dx, N);
	
	double *work=(double*)malloc(sizeof(double)*(*N*(*dx)));

	for(i=0;i<*ns;i++)
	{
		Scrambling(os1);
	
		getPoints(os1, dx, N, work);

		for(k=0;k< *N*(*dx);k++)
		{
			x[*N*(*dx)*i+k]=work[k];
		}
	}

	free(work);
	work=NULL;
	Scrambled_Destroy(os1);
	os1=NULL;
}


void QMC (int *qmc, int *N, int *dx, double* x)
{

	DigitalNetGenerator *os1=DigitalNetGenerator_Create(&qmc, dx);

	DigitalNetGenerator_GetPoints(os1, dx, N, x); 

	DigitalNetGenerator_Destroy(os1);
	os1=NULL;
}


























